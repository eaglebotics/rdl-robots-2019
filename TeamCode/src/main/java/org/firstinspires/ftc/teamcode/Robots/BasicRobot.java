package org.firstinspires.ftc.teamcode.Robots;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

public abstract class BasicRobot extends LinearOpMode {
        /*
        All of the following private static variables
        are designed to work with the hardware.
    */

    // Timer
    static ElapsedTime timer;

    static DcMotor leftFrontWheel, rightFrontWheel, leftBackWheel, rightBackWheel; // Direct-current motors
    static DcMotor lift;

    /*
        The following private methods are meant to interface
        with the above private variables.
    */

    // Power = current applied to motor (1 = max, 0 = off)
    private static void leftSide(double pwr) {
        leftFrontWheel.setPower(pwr);
        leftBackWheel.setPower(pwr);
    }
    // Controls the whole right side of the robot
    private static void rightSide(double pwr) {
        rightFrontWheel.setPower(pwr);
        rightBackWheel.setPower(pwr);
    }

    static void moveLift(double pwr) {
        lift.setPower(pwr);
    }

    // 1 = max power, 0 = zero power (off)
    static void setPowers(double left, double right) {
        leftSide(left);
        rightSide(right);
    }
    static void setPowers(double both) {
        setPowers(both, both);
    }

    static void moveForward(double pwr) { setPowers(-pwr, pwr); }
    static void moveForward(int time, double pwr) {
        ElapsedTime t = new ElapsedTime();
        t.reset();
        while (t.time() < time) {
            moveForward(pwr);
        }
        setPowers(0);
    }
    static void moveBackward(double pwr) { setPowers(pwr, -pwr); }
    static void turnLeft(double pwr) { setPowers(pwr); }
    static void turnRight(double pwr) { setPowers(-pwr); }

    /*
        The following protected methods are designed to
        be called from within an OpMode class.
    */

    protected void initHardware() {
        leftFrontWheel = hardwareMap.dcMotor.get("tl"); // t = top, l = left
        rightFrontWheel = hardwareMap.dcMotor.get("tr"); // r = right
        leftBackWheel = hardwareMap.dcMotor.get("bl");
        rightBackWheel = hardwareMap.dcMotor.get("br");
        lift = hardwareMap.dcMotor.get("l");
    }

    protected abstract void teleOp();
    protected abstract void auto();

}
